import api from '../api/create';




function getAllPost() {
    return api.get("/posts");
}

function getPostById(id) {
    return api.get("/post/"+id)
    
}


export {
    getAllPost,
    getPostById
}